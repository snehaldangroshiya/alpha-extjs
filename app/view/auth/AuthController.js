Ext.ns('Alpha.view.auth');
Ext.define('Alpha.view.auth.AuthController', {
	extend: 'Ext.app.Controller',
	alias: 'controller.authtcontroller',

	requires: ['Alpha.view.*'],
	/**
	 *
	 */
	constructor : function (config) {
		this.callParent(arguments);
	},

	/**
	 *
	 */
	doInit : function () {
		this.listen({
			component: {
				'[xtype=loginpanel] button#login': {
					click: this.doLogin
				}
			},
			global: {
				beforeviewportrender: this.processLoggedIn
			}
		});
	},

	processLoggedIn : function () {
		//Ext.globalEvents.fireEvent( 'aftervalidateloggedin' );
		container.getMainPanel();
		//Ext.widget('loginpanel').show();
		
	},

	doLogin : function( button, e, eOpts ) {
		var win = button.up( 'panel' );
		var form = win.down( 'form' );
		var values = form.getValues();
		Ext.Ajax.request({
			url: 'http://localhost:8000/api/v1/user/login',
			method: 'POST',
			jsonData : {
				"username": values.username,
				"password": values.password
			},
			success: function( response, options ) {
				var result = Ext.decode(response.responseText);
				Ext.globalEvents.fireEvent( 'aftervalidateloggedin' );
				win.close();
				container.getMainPanel();
			},
			failure: function( response, options ) {
				Ext.Msg.alert( 'Attention', 'Sorry, an error occurred during your request. Please try again.' );
			}
		});
	}
});
