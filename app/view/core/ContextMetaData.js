Ext.ns('Alpha.view.core');
/**
 * @class Alpha.view.core.ContextMetaData
 * @extends Alpha.view.core.PluginMetaData
 *
 * The Meta Data object containing the registration details
 * of a {@link Alpha.view.core.Context}. An instance of this object
 * must be passed to {@link Alpha.view.core.Container#registerContext}.
 */
Ext.define('Alpha.view.core.ContextMetaData', {
    extend : 'Alpha.view.core.PluginMetaData',
	/**
	 * @constructor
	 * @param {Object} config Configuration object
	 */
	constructor : function(config)
	{
		config = config || {};

		Ext.applyIf(config, {
			// By default Contexts cannot be disabled
			allowUserDisable : false
		});

		this.callParent(arguments);
	}
});
