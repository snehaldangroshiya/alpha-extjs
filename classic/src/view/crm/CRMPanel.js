Ext.namespace('Alpha.view.crm');

Ext.define('Alpha.view.crm.CRMPanel', {
	extend : 'Ext.panel.Panel',
	alias : 'widget.erp.crmpanel',
	/**
	 * @constructor
	 * @param config Configuration structure
	 */
	constructor : function(config)
	{
		config = config || {};

		Ext.applyIf(config, {
			xtype: 'crmpanel',
			layout  : 'fit',
			border : false
			//items : this.initHotelGrid(config.context)
		});
		this.callParent(arguments);
	}
});
