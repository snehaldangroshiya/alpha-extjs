Ext.ns('Alpha.view.crm');
/**
 *
 */
Ext.define('Alpha.view.crm.CRMContext', {
	/**
	 *
	 */
	extend : 'Alpha.view.core.Context',
	/**
	 *
	 */
	requires :[
		'Alpha.view.crm.*'
	],

	/**
	 *
	 */
	constructor: function (config)
	{
		this.callParent(arguments);
	},

	/**
	 * Override this method to return a new instance Ext.Panel.
	 * This instance will be placed at the center of the screen when the
	 * context is active.
	 * <p>
	 * The default implementation of getComponents() calls this method to
	 * lazily construct the toolbar.
	 * @return {Ext.Panel} a new panel instance
	 */
	createContentPanel : function()
	{
		return {
			xtype : 'erp.crmpanel',
			context : this
		};
	}
});

Alpha.onReady(function(){
	container.registerContext(new Alpha.view.core.ContextMetaData({
		name : 'crm',
		displayName: 'CRM',
		cls : 'erp erp-glasss',
		allowUserVisible : false,
		pluginConstructor : Alpha.view.crm.CRMContext
	}));
});