Ext.ns('Alpha.view.mail');
/**
 *
 */
Ext.define('Alpha.view.mail.MailContext', {
	/**
	 *
	 */
	extend : 'Alpha.view.core.Context',
	/**
	 *
	 */
	requires :[
		'Alpha.view.mail.MailContextController',
		'Alpha.view.mail.MailContextViewModel',
		'Alpha.view.mail.*'
	],

	/**
	 *
	 */
	controller: 'mailcontextcontroller',

	/**
	 *
	 */
	viewModel: 'mailcontextviewmodel',

	/**
	 *
	 */
	constructor: function (config)
	{
		Alpha.view.mail.MailContext.superclass.constructor.call(this, config);
		this.registerInsertionPoint('navigation.center', this.createContactNavigationPanel, this);
	},

	/**
	 * Override this method to return a new instance Ext.Panel.
	 * This instance will be placed at the center of the screen when the
	 * context is active.
	 * <p>
	 * The default implementation of getComponents() calls this method to
	 * lazily construct the toolbar.
	 * @return {Ext.Panel} a new panel instance
	 */
	createContentPanel : function()
	{
		return {
			xtype : 'erp.mailpanel',
			context : this
		};
	},
	/**
	 *
	 * @returns {{xtype: string, context: Alpha.view.mail.MailContext, items: *[]}}
	 */
	createContactNavigationPanel : function()
	{
		return {
			xtype : 'contextnavigation',
			context : this,
			items : [{
				xtype : 'panel'
			}]
		};
	}
});

Alpha.onReady(function(){
	container.registerContext(new Alpha.view.core.ContextMetaData({
		name : 'mail',
		displayName: 'Mail',
		cls : 'erp erp-glasss',
		allowUserVisible : false,
		pluginConstructor : Alpha.view.mail.MailContext
	}));
});