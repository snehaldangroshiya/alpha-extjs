Ext.namespace('Alpha.view.auth');

Ext.define('Alpha.view.auth.Auth', {
	/**
	 * 
	 */
	extend : 'Ext.container.Viewport',

	/**
	 *
	 */
	alias : 'widget.loginpanel',

	/**
	 *
	 */
	viewModel: 'authviewcontroller',

	/**
	 * @constructor
	 * @param config Configuration structure
	 */
	constructor : function(config)
	{
		config = config || {};
		Ext.applyIf(config, {
			layout : {
				type : 'vbox',
				align : 'stretch'
			},
			items : [{
				xtype : 'container',
				flex : 0.5
			},{
				layout : {
					type : 'hbox'
				},
				xtype : 'container',
				items : [{
					xtype : 'container',
					flex : 0.5
				},{
					xtype : 'panel',
					title : 'Login',
					titleAlign : 'center',
					bodyPadding: 10,
					border : false,
					items : [{
						xtype : 'form',
						width : 250,
						layout : 'anchor',
						items : [{
							xtype : 'textfield',
							hideLabel : true,
							//padding : '0 0 5 0',
							anchor : '100%',
							name : 'username',
							emptyText : 'User Name',
							allowBlank: false
						}, {
							xtype: 'textfield',
							hideLabel : true,
							name : 'password',
							anchor : '100%',
							emptyText : 'Password',
							inputType: 'password',
							allowBlank: false
						}]
					}],
					buttons: [{
						text: 'Login',
						flex :1,
						itemId: 'login',
						scope: this
					}]
				},{
					xtype : 'container',
					flex : 0.5
				}]
			},{
				xtype : 'container',
				flex : 0.5
			}]
		});
		this.callParent(arguments);
	}
});
