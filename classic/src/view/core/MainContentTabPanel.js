Ext.namespace('Alpha.view.core');

/**
 * @class Alpha.core.ui.MainContentTabPanel
 * @extends Ext.container.Container
 *
 * This subclass is used in the main content area, and contains the ContextContainer and dialogs that the user opens
 * Initialized in MainViewport.CreateContentContainer
 */
Ext.define('Alpha.view.core.MainContentTabPanel', {

	extend : 'Ext.container.Container'
});
