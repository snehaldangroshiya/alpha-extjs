Ext.ns('Alpha.view.core');
Ext.define('Alpha.view.core.ContextNavigationPanel', {
	/**
	 *
	 */
	extend: 'Ext.panel.Panel',
	/**
	 *
	 */
	alias : 'widget.contextnavigation',
	/**
	 * For this Context this panel will be visible in the {@link Alpha.core.ui.NavigationPanel NavigationPanel}.
	 * @cfg {Alpha.core.Context} Related Context
	 */
	context: null,

	/**
	 * @constructor
	 * @param {Object} config configuration object
	 */
	constructor : function (config) {
		config = config || {};

		// Config options for component itself.
		Ext.applyIf(config, {
			border : false,
			layout: 'fit',
			defaults : {
				border : false,
				autoScroll : false,
				//defaults : { cls : 'zarafa-context-navigation-item-body' }
			}
		});

		this.callParent(arguments);
	},

	/**
	 * @return {Alpha.core.Context}
	 */
	getContext : function() {
		return this.context || false;
	}
});