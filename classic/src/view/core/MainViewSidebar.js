Ext.namespace('Alpha.view.core');

Ext.define('Alpha.view.core.MainViewSidebar', {
	/**
	 *
	 */
	extend: 'Ext.panel.Panel',
	/**
	 *
	 * @param config
	 */
	constructor : function(config)
	{
		config = config || {};

		Ext.applyIf(config, {
			cls: 'shadow-panel',
			border : false,
			collapsible : true,
			//collapsed : false,
			split : true,
			width : 242,
			minSize : 150
		});

		this.callParent(arguments);
	}
});